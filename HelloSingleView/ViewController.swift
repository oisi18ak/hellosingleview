//
//  ViewController.swift
//  HelloSingleView
//
//  Created by Simon Oinonen on 2019-10-22.
//  Copyright © 2019 Simon Oinonen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func onClickButton(_ sender: Any) {
        print("Hello button")
        
    }
    
}

